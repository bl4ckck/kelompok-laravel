<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Order::all();
        return view('order.index', compact('data'));

        //PAGINATION
        // $data = Customer::latest()->paginate(5);

        // return view('customer.index', compact('data'))
        //     ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate requests
        $request->validate([
            'order_date' => 'required',
            'total_price' => 'required',
            'paid' => 'required',
        ]);

        Order::create(
            $request->all()
        );

        return redirect()->route('dashboard')
                        ->with('success','Order successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $data = Order::find($order->id_order)->orders;

        return view('order.show', compact('data', 'order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('order.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        // Validate requests
        $request->validate([
            'order_date' => 'required',
            'total_price' => 'required',
            'paid' => 'required',
        ]);

        $order->update(
            $request->all()
        );

        return redirect()->route('dashboard')
                        ->with('success','Order successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
        $order->delete();

        return redirect()->route('dashboard')
                        ->with('success','Order deleted successfully');
    }
}
