<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::resource('customer', CustomerController::class)->middleware(['auth']);

Route::resource('order', OrderController::class)->middleware(['auth']);

Route::get('dashboard', function (){
    return (new CustomerController())->index();
})->middleware(['auth'])->name('dashboard');

Route::get('/dashboard2', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard2');

require __DIR__.'/auth.php';
