<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            'id_order' => 1,
            'order_date' => date("Y-m-d"),
            'total_price' => random_int(50000, 1000000),
            'paid' => random_int(50000, 1000000),
        ]);
    }
}
